/**
 * This is not sequential execution of promises. Just the order of the result is maintained.
 * @param {Array} arrayOfPromises Is the array of promises passed to this method which will return a new promise
 * @param { Object } optionsObject Is the additional parameter passed which contains critical Index which is of type number.
 * @return {Promise} Which can be utilised and the reponse will be in the same order in an array in which the promises were index in the passed array.
 */
const PromiseAllSettled = (arrayOfPromises = [], optionsObject = {}) => {
  const { critcalIndexes = [] } = optionsObject;
  if (Object.prototype.toString.call(arrayOfPromises) !== "[object Array]")
    return false;
  return new Promise((resolve, reject) => {
    const resultArray = [];
    if (!arrayOfPromises.length) resolve(arrayOfPromises);
    let pendingPromises = arrayOfPromises.length;
    arrayOfPromises.forEach((promise, index) => {
      promise
        .then((result) => {
          resultArray[index] = {
            value: result,
            status: "resolved",
            resolved: true,
          };
          pendingPromises -= 1;
          if (pendingPromises === 0) resolve(resultArray);
        })
        .catch((error) => {
          if (!critcalIndexes.length || critcalIndexes.includes(index))
            reject(error);
          resultArray[index] = {
            value: error,
            status: "rejected",
            resolved: false,
          };
          pendingPromises -= 1;
          if (pendingPromises === 0) resolve(resultArray);
        });
    });
  });
};

module.exports = PromiseAllSettled;
